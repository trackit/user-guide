Administration
==============
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

.. _admin:

==============
Admin Overview
==============

TRACKIT provides access to all features and functionality within the tool through the Administration section. Only Admin Users should be setup to have permissions to access these functions as they can determine the way TRACKIT works.

Do not update any settings in this section without confidence in what is being acheived or without guidance from TRACKIT Support or Services.


======================
Administration Options
======================

Below is a list of each Admin section and a summary of the contents.

-------------------------
Access and Authentication
-------------------------

Allows updating of the Users and Groups.


----------------
Asset Management
----------------

This section covers everything to do with managing the assets, groups, envrionments, contracts and mount locations.


-----------
Audit Trail
-----------

An administrative view of the audit trail for everything within TRACKIT.


---------
Constance
---------

This Config provides access to global system settings, usually this is pre-configured, however it's possible settings could be changed here if directed by TRACKIT Support or Services.


------------
Custom Forms
------------

This should not be used.

--------
Djcelery
--------

This section manages tasks and workflows within TRACKIT, it is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.


------------------------
Dynamic Model Validation
------------------------

This section manages rules and validation within TRACKIT, it is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.


---
EAV
---

This provides access to the EAV fields. It's possible to add, modify or remove EAV fields and groups in this section.


-----
Filer
-----

This section manages folders within TRACKIT, it is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.


-----------------
Import and Export
-----------------

This section manages the import and export of data within TRACKIT, it is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.

-------------------
Location Management
-------------------

Many Location functions are available in this section.

* Location Cell Names - Provides the ability to modify cells mappings for Floorplans with grids and cells.
* Location Drawings - Allows the viewing/modification or removal of Floorplan drawings that have been added to Locations.
* Location Images - Allows the viewing/modification or removal of images that have been added to Locations.
* Location Mappings - Allows the viewing/modification or removal of Floorplan mappings, such as Grid definitions, that have been added to Locations spaces.
* Location Types - Full access to Location Types
* Locations - Full access to Locations
* Region - Provides functionality to define Regions
* Region Types - Allows Region Groups to be added/modified or removed.


----------------------
Monitoring and Metrics
----------------------



-----
Power
-----

This section is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.

-----------------------
SVG Drawing/Annotations
-----------------------

This section is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.

----------
Token_Auth
----------

This section is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.

-------
Trackit
-------

This section is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.

--------------------
Trackit External API
--------------------

-----------------------
Trackit Product Library
-----------------------

-----------------
Trackit Reporting
-----------------

---------
Valuation
---------

-------
Wizards
-------

-------------------
Workflow and States
-------------------

This section is preconfigured and should not be tampered with except under guidance from TRACKIT Support or Services.