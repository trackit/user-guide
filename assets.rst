Assets
======
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

==============
Asset Overview
==============

Assets can be added against either Locations or Parent Assets. 

Data can added, edited and removed in a number of ways:

#. Using the TRACKIT Web Client.
#. Added via the TRACKIT Import :ref:`asset_import_wizard`.
#. Via the TRACKIT RESTful API 


.. _placement_types:

===============
Placement Types
===============

There are a number of methods of placing Assets in TRACKIT.

==================== ============= =================== ============ ============== ======== ==================== ========== =============
Placement Type       Main Location Cell/Grid Placement Parent Asset Mount Location Offsets  Rotation/Orientation U Position Mounting Area
==================== ============= =================== ============ ============== ======== ==================== ========== =============
Floor Mounted        Required      Required                                        Required Required   
Rack/Cabinet Mounted Required                          Required     Required       Required Required             Required    
Asset Mounted        Required                          Required                             Required                        Required                          
Basic Placement      Required                          Optional
==================== ============= =================== ============ ============== ======== ==================== ========== =============



=============
Adding Assets
=============

The Add Asset button is available in a number of places through TRACKIT, usually in the :ref:`sidebar`. Once selected, the Add Asset Form is presented, the User can complete this to Add an Asset to the database.

The Add Form comprised 3 parts:

#. Step One - Basic Details and Placement Type 
#. Step Two - Product and Placement/Parent Details
#. Step Three - Additional Information


.. _basic_details:

-------------
Basic Details
-------------

This first step asks for some basic information on the Asset Name, any Serial or Asset Numbers.

It also requests the User add what Placement Type to use. This will determine what Products and Placement detail to collect on Step 2.

.. figure:: ./assets/add_asset_step1.png
    :align: center
    :alt: Header Menu

    Step 1 of adding an Asset showing the different Placement Types


-----------------------------
Product and Placement Details
-----------------------------

The next step requires the User to select the Product to use for the Asset.

Depending on which Placement Type was selected, any additional :ref:`placement_types` data might be required at this step.

.. figure:: ./assets/add_asset_placement.png
    :align: center
    :alt: Header Menu

    An example of Floor Placement required fields in Step 2 of adding an Asset


----------------------
Additional Information
----------------------

The final step allows the User to complete additional data fields. These typically include EAV fields for Assets.

The Add Asset Form will validate the data at each step and warn the User if there are any issues. Once completed, the Asset will be added to the system.


.. figure:: ./assets/add_asset_validate.png
    :align: center
    :alt: Header Menu

    An example of form validation on adding an Asset


==============
Editing Assets
==============




-----------------
Edit Asset Button
-----------------

When viewing an Asset, the Edit Asset button is visible in the :ref:`sidebar`. This will take the User through the 3 steps of the :ref:`basic_details` Add Asset form, however any existing data is pre-populated.


.. figure:: ./assets/edit_asset_history.png
    :align: center
    :alt: Header Menu

    Viewing the details of an Asset Edit in the History tab.


------------
Assets Table
------------

It's possible with appropriate permissions to edit Assets in the :ref:`content_assets` Table.


.. figure:: ./assets/edit_assets_grid.png
    :align: center
    :alt: Header Menu

    An example of editing an Asset in the Asset Table.



===============
Deleting Assets
===============

Assets can be deleted from TRACKIT, however they cannot be restored once deleted.

It is highly recommended that Assets are instead moved to a Decommissioned or Remove Location which can be reported on and retains the History of the Asset through it's lifecycle.

-------------------
Delete Asset Button
-------------------

Should it be a requirement to delete an Asset, it can be done by selecting the Delete Asset button visible in the :ref:`sidebar` when viewing an Asset.

.. figure:: ./assets/delete_asset.png
    :align: center
    :alt: Header Menu

    Confirmation and Warning message when Deleting an Asset.

Deleting an Asset will remove all Child Assets and should only be done where there is an error and/or the History of the Asset is not required.



