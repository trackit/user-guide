EAV (Entity Attribute Values)
============================
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

.. _eav_overview:

============
EAV Overview
============


.. _eav_group:

==========
EAV Groups
==========

-----
Order
-----

This defines the priority order within which to show the Attribute Groups on the forms within which they're displayed.

----
Name
----

The name of the EAV Group.

---------------
Bootstrap Class
---------------

This determines the width of the field, if Small or Extra Small are chosen, the field will consume the full width of the screen (designed for small screens). Medium and Large are the opposite.

* Extra Small - Full width
* Small - Full width
* Medium - Half width
* Large - Half width

.. _eav_attributes:

==============
EAV Attributes
==============

----
Name
----

The name of the field.

-----
Order
-----

The priority order to display the field.

----
Slug
----

A unique name, which does not allow spaces

--------
Required
--------

Is the field required when the object is added. A validation error will appear if no :ref:`eav_default_value` is entered.

--------------------
Include in Main Grid
--------------------

If this option is checked, the field will appear in the :ref:`content_assets` grid table.

-------------------
Include in Sub Grid
-------------------

If this option is checked, the field will appear in the :ref:`content_assets` expanded grid table, i.e. where a Parent Asset has been expanded and the child Assets are listed.

-----------
Description
-----------

Some text which will be displayed below the EAV field to help Users identify what the field is for.

.. figure:: ./eav/eav_description.png
    :align: center

    An example of the description text underneath the EAV field.

---------------
Attribute Group
---------------

The name of the :ref:`eav_group` which the field should belong to. The field can only be a member of one group.


.. _data_type:

---------
Data Type
---------

The type of data that can be stored in this field. The following options are available:

* Text - A string of text
* Float - A number or decimal
* Integer - A whole number
* Date - A datetime value
* True/False
* Multiple Choice - If chosen, the :ref:`choice_group` field must be populated.
* Distance Field

----
Type
----

This determines what object type the field belongs to, it can be either:

**Asset** or **Location**.

.. _choice_group:

------------
Choice Group
------------

It is possible to create and edit Choice Groups. If the Multiple Choice option has been selected under :ref:`data_type`, this field must have a value.

.. figure:: ./eav/eav_choice_group.png
    :align: center

    A selected Choice Group, showing the Edit Group option open in a new window.


.. _eav_default_value:

-------------
Default Value
-------------

A default value, if required. This could be ``0`` or ``None``.
