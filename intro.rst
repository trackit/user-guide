Intro
=====
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

=====
About
=====

TRACKIT is an Asset Management tool, designed to be delivered via the Cloud as a Software as a Service (SaaS) application. It can be deployed locally and installed on a local virtual or physical machine within a Customer environment.

-----------
PRO Version
-----------

TRACKIT PRO is an Enterprise level version of TRACKIT offering additional functionality such as a Scalable IT Equipment Library, Network and Power Connectivity and cable tracing. In addition, there are many integrations to Enterprise platforms built and configured by TRACKIT Service Partners available.


===========================
Asset Management in TRACKIT
===========================

TRACKIT has been designed to provide an accurate, easily maintainable inventory of Locations and Assets. This could be any size, such as a Small Office environment or Store Room, through to a Large Enterprise scale Data Centre. Applications include Vehicle Fleet Management, IT Asset Management, Warehouse Stock Inventories or Mobile Equipment Monitoring.

---------
Locations
---------

Locations are designed to contain either additional child Locations, such as a Building containing Floors and Rooms, and within these Assets can be placed. Locations could be physical such as a Building, or logical, such as an organisational unit or department.

------
Assets
------

Assets are intended to be anything which requires managing, all Assets have a history, can be added, modified, removed. Assets can be placed within Locations or other Assets using various placement or mount types. To create an Asset, a Product must be created which determines all the physical attributes of the Asset.

.. _products:

--------
Products
--------

Products are templates or symbols of any given Asset usually grouped by the Manufacturer of the Product and the Model of the Asset. They include, but are not limited to physical attributes, such as dimensions and weight and could include information such as graphical images, interfaces such as ports and connectivity or financial details such as cost or warranties. Anything could be defined against a Product.

Products should belong to an Asset Class, and can belong to more than one class if required.


.. _asset_class:

-----------
Asset Class
-----------

Products belong to an Asset Class, the following classes are system classes and define :ref:`placement_type`, they should not be removed:

	* Floor Mountable - Allows Assets to be mounted on a Floor
	* Rack/Cabinet Mountable - Allow Assets to be mounted within a Rack/Cabinet Asset with U Space (for IT cabinets).
	* Device/Instance Mountable - Allow Assets to be mounted within other Assets.
	* Blade Servers - Allow Assets to be mounted within Asset Slots (for IT Assets)

The remaining are default classes, which can be altered or removed if required:

	* IT equipment
	* Software and Licenses
	* Furniture and fixtures
	* Machinery
	* Office equipment
	* Vehicles
	* Intangible assets

Asset Classes determine some behavior of Assets within TRACKIT, for instance, how Assets can be mounted in Locations such as on the Floor, within other Assets or whether they can be mobile or not.


.. _placement_type:

--------------
Placement Type
--------------

To ensure Assets are placed correctly, the Product Model must belong to an appropriate system :ref:`asset_class`.

It is possible for a Product to belong to more than one Asset Class, therefore ensure that the correct class is chosen to determine the placement type, and add any additional classes for reporting/grouping as required.


.. _categories:

----------
Categories
----------

Categories are the type of Asset which a Product belongs to, the Product must belong to a Sub Category, which belongs to a Parent Category. 

For example, if a Car was being added to TRACKIT, the following configuration would be used:

	* Manufacturer: Ford
	* Model: GT Mustang
	* Asset Class: Vehicles
	* Category: Car
	* Sub-Category: Performance

Another example, such as a Computer could use the following setup:

	* Manufacturer: Hewlett Packard
	* Model: Envy 15-AS104NA
	* Asset Class: IT equipment
	* Category: Computers
	* Sub-Category: Laptops



---------------
Additional Data
---------------

Any additional data can be added to TRACKIT either as additional data fields, called :ref:`eav_overview` and imported by the User or through an Integration to another product. Examples of additional data are:

* Procurement Information, Warranties, Purchase Dates, Asset Values
* GPS Information, visually showing where an Asset is on a live map or showing its path over time.
* Power Information - Current and historic power consumption of an Asset from intelligent data sources.
* Environmental Information - Power, Heat, Humidity information from a Buiding Management System

