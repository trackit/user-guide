Layout
======
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

==================
Interface Overview
==================

TRACKIT has a responsive layout, provided the browser is modern and supports WebKit, users can access the application. On entering a valid username and password into the login screen, users will be greeted with the standard application interface detailed below.

* Header Menu

	* Global Search
	* View Layout
	* Account Settings

* Sidebar
* Dashboard
* Location/Asset Breadcrumb
* Location/Asset Information


.. _header_menu:

===========
Header Menu
===========

The Header Bar is a persistent feature that appears at the top of the application throughout TRACKIT.

.. figure:: ./layout/header_menu.png
    :align: center
    :alt: Header Menu

    How the Header Menu appears within TRACKIT.

-------------
Global Search
-------------

This function allows users to search for Assets, Locations and Products. The results are returned in a list with the most relevant matches first. It's possible to refine the search using the Asset Type filter in the  :ref:`sidebar`.


.. _view_layout:

-----------
View Layout
----------- 

This alters the view of the :ref:`location_asset_info`. 

Sections can either be displayed as Tabs, as Horizontally or Vertically split sections on a single page, or a specific Image/Floorplan and Asset Grid view is available.


.. _account_menu:

------------
Account Menu
------------
Provides access to user settings, Administation (with appropriate permissions)


.. _sidebar:

=======
Sidebar
=======

The Sidebar contains controls and tools relevant to the main content. It can be hidden or pinned to maximise content space if required. Depending on permissions and content, the following features may be available:

*	Add/Edit/Delete Asset 
*	Location Tree
*	Product Search
*	Asset Grouping
*	Date Range Selectors


.. _dashboard:

=========
Dashboard
=========

The Dashboard is generally displayed as the landing page of TRACKIT once a user logs in. Typically this includes widgets and a Location Map.

------------
Location Map
------------ 
Displays a global map (external Google access required) of all locations with address details.


.. _location_asset_breadcrumb:

=========================
Location/Asset Breadcrumb
=========================

The breadcrumb beneath the Header Bar on most content pages within the application. It displays the current location within the location hierarchy and all the parent and grandparent objects. It's possible to drill further into child objects.

All objects displayed within the breadcrumb are hyperlinked and can be selected and opened, either in a new tab should the browser support it, or directly in the current window.


.. _location_asset_info:

===============================
Location/Asset Information Tabs
===============================

This area contains all the content and information stored against either the Location or Asset selected.

Using the :ref:`view_layout`, the user can swap between a tabbed layout, a horizontal layour or a vertical layout. There is also an option to view only the SVG element and the Asset Grid.

Depending on the informaiton being stored, the following sections are available:

.. _content_details:

-------
Details
-------

This section shows simple Asset or Location details, such as object name, code, asset/serial numbers and summary details relevant to the object, for instance an asset count for Locations.

.. _content_assets:

------
Assets
------

A table of all assets within either the Asset or Location selected. Any child assets of the listed assets can be viewed by expanding the row to show a nested table.

This view can be ordered by selecting a column header, filtered by using the text box immediately above the table and if the relevant permissions are available assets can be edited inline.


.. _content_reports:

-------
Reports
-------

Reports appropriate to the selected object are listed in this section. For example, if a Location this would be a Full Asset List and an Audit Trail report. Any reports built by and/or shared by Users are listed here also.

Reports can be run by simply selecting the report name, where date ranges are appropriate, some pre-selected ranges are listed adjacent to the report name.

.. _content_history:

-------
History
-------

The entire audit log of either a Location or an Asset is viewable in this section. There are date selectors to choose the date range to search between. 

The information available includes:

*	Client Type - How the update was made, via the web interface, or via an API (i.e. an integration to another system)
*	Action Type - Was the update modifying an object, or adding or removing an object.
*	Timestamp - The server timestamp of when the change was made to the database.
*	Model Type - What type of object was updated.
*	User - The username of the User who made the update.
*	Server Host Name - Name of the server which registered the change.
* 	IP Address - The IP of the client from with the change was made.

Where a modification to existing data has occured, a hyperlink is available which can be selected to show the fields modified and the before and after values.


.. _content_floorplan:

----------
Floor Plan
----------

This section requires the object selected to be a Location. If a floorplan has been defined, it can be visualised here. 

The floorplan may include layers from an AutoCAD dwg, an Adobe PDF, a Microsoft Visio or from and SVG. It may include a defined floor space, which may be gridded.

Floorplans can be dragged to pan around, zoom in and out of and layers switched on and off.

It's also possible to drag and drop Products which can be mounted on the floor into the correct physical location in this view.


.. _content_image:

-----
Image
-----

If the object selected is an Asset and an SVG image is available, this would visualised here.


.. _content_photos:

------
Photos
------

Photos of Assets can be uploaded through either a web browser, or are added automatically via the TRACKIT Mobile App.


.. _content_notes:

-----
Notes
-----

Notes added by Users can be viewed in this section. Each note includes the User, Timestamp and note content.


.. _content_tasks:

-----
Tasks
-----

Any tasks, either current or historical are listed under this tab.


.. _content_monitoring:

----------
Monitoring
----------

If any Monitoring or Metric information have been linked to either and Asset or Location, it will be viewable here. Metrics can be selected from the list and the data values and their respective details can be viewed on the subsequent page.

.. _content_alerts:

------
Alerts
------

If any Alerts have been configured against any Metrics linked to the object, they will be shown in this section. There is an option to toggle between viewing Archived or Current Alerts only.


.. _content_tracking:

--------
Tracking
--------

Depending on connectivity, should any GPS data have been entered or populated against an Asset, it will be shown visually on a map in this section. On an individual Asset, this would include a time slider at the top of the map allowing the User to alter the range within which to display the GPS data.

If a Location has been selected containing multiple Assets, the last data point recieved for each Asset holding GPS data will be shown on the map, with labels identifying each point.




