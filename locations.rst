Locations
=========
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

==================
Locations Overview
==================

Locations can be created in the :ref:`location_wizard`.. Ensure the correct :ref:`location_type`. exists first, and consider how the Location structure should appear.

Typically, a structure would look something like the image below, however it could be generated to represent any parent > child relationship between objects:

.. figure:: ./locations/location_structure.png
    :align: center
    :alt: Header Menu

    An example of a simple Location structure.

Ensure the correct properties are assigned to a Location Type, for example, the a Building type has the ability to accept Floor and/or Rooms as Child Location Types.

It is possible to amend a Location Structure at any time using the :ref:`location_tree`, so it can be updated as required if the model structure isn't appropriate.


================
Location Details
================

Location details can be updated using the :ref:`location_wizard` or the Edit Location button in the :ref:`sidebar` when viewing a Location.

Any Location fields, including EAV fields can be updated on this form.


==============
Edit Floorplan
==============

It is possible to create a visual floorplan of a Location in the following situations:

* If the User wished to define a new area.
* If data has been imported from TRACKIT Mobile.
* If data has been imported from a previous version of TRACKIT.
* If data has been imported from another product, such as a CMDB or DCIM tool which has space information.


------------------------
Importing Floorplan Data
------------------------

If existing data is available in a support format, this can be added to a TRACKIT Location.

Supported formats are as follows:

* AutoCAD DWG - All valid layers will be imported and the scale retained.
* Microsoft Visio - Everything is imported as a single layer which would require scaling to the correct size.
* Adobe PDF - Everything is imported as a single layer which would require scaling to the correct size.
* Scalable Vector Graphics (SVG) - Imported as a single layer which would require scaling to the correct size.


Floorplans can be imported by simply dragging and dropping from a Desktop or File Explorer on the Users PC into the web browser.


.. figure:: ./locations/location_floorplan_import.png
    :align: center
    :alt: Header Menu

    The import screen of a Floorplan

Once the file has been imported, it can be scales, rotated or moved as appropriate. 

If the User requires, they can create an area on the floorplan to allow mounting of Asset using :ref:`border_path`.


.. _border_path:

===============
Edit Borderpath
===============

If a floorplan has been added, it's possible to define an area on the floorplan to designate as a set Locations mounting area.

To do so, select the Edit Border Path button on the Edit Floorplan form.

Once the floorplan has loaded, choose the option on the :ref:`sidebar` to add points, creating enough to surround the area accurately.

.. figure:: ./locations/location_border_path.png
    :align: center
    :alt: Header Menu

    The import screen of a Floorplan


=======
Regions
=======

If the Map is used on the Dashboard screen, regions can be defined to place locations into. Regions are created in the Administration menu, under Location Management.