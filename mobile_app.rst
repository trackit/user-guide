Mobile Apps
===========
.. versionadded:: 16.02

.. image:: https://img.shields.io/badge/release-16.02_alpha-orange.svg

=======================
TRACKIT Mobile Overview
=======================

TRACKIT is an Asset Management tool, designed to be delivered via the Cloud as a Software as a Service (SaaS) application. It can be deployed locally and installed on a local virtual or physical machine within a Customer environment.


==================
TRACKIT Mobile App
==================

TRACKIT has been designed to provide an accurate, easily maintainable inventory of Locations and Assets. This could be any size, such as a Small Office environment or Store Room, through to a Large Enterprise scale Data Centre. Applications include Vehicle Fleet Management, IT Asset Management, Warehouse Stock Inventories or Mobile Equipment Monitoring.

.. figure:: ./mobile/trackit_app.png
    :align: center
    :alt: Header Menu

    A view of the TRACKIT MOBILE App.



==============
TRACKIT Mobile
==============

TRACKIT Mobile is a Windows tablet based application designed for Offline Data Collection of IT Rooms and Datacentre Assets.

.. figure:: ./mobile/trackit_mobile.png
    :align: center
    :alt: Header Menu

    A view of TRACKIT Mobile.

