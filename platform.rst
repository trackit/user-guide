Platform and Installation
=========================
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

=====================
Installation Overview
=====================

Depending on the deployment, TRACKIT will have been either setup in the Cloud automatically, and can be accessed via a personalised url, i.e. `https://mycompany.trackit.cloud/ <#>`_, or it will have been installed locally via a TRACKIT Engineer. For details on local installations please see :ref:`installation_steps`.

Please contact your Administrator if you are unsure which installation type you have.


==================
Cloud Installation
==================

There are no installation steps required, the application link will be delivered via email.


==========================
Local Installation via Web
==========================

-----------------------
Hardware Pre-requisites
-----------------------

The TRACKIT architecture is container based. Containers can be placed on the same server or node, or split across multiple nodes. Typically, TRACKIT runs on Linux nodes, with a dedicated Reporting node running Windows. TRACKIT is database and web server agnostic, however we highly recommend PostgreSQL or Oracle as a database platform.

---------------------
Optimum Specification
---------------------

The following is a recommended node specification for running the TRACKIT application utilising all features and functionality in a multiple location/user environment.

* Intel Xeon E5-2440 hexa-core 2.4 GHz or equivalent processor (dual processor if supported by chassis)

* 16GB DDR3 Memory

* 144GB HDD RAID1 minimum recommended

* Dual Gigabit NIC

Recommended Nodes:

* 2x Linux Based OS (Ubuntu 16.04) nodes

* 1x Windows Based OS (Windows 2016 Server Edition) node

External VPN Access for support/maintenance purposes (recommended)

Installation requires and internet connection, if this is not available please see the :ref:`offline_installation` section for further details.



------
Docker
------

TRACKIT uses Docker as the platform container.

Information on Docker can be found here: https://www.docker.com/ and a list of support Operating Systems can be found here: https://www.docker.com/community-edition



.. _installation_steps:

-------------------------------
Installation Steps - Linux Node
-------------------------------

^^^^^^^^^^^^^^
Install Docker
^^^^^^^^^^^^^^

Install Docker for Ubuntu, instructions found here: https://docs.docker.com/engine/installation/linux/ubuntu/

Install Docker as per standard Ubuntu instructions. Ensure you add your system user to the docker group: ::

   	usermod -aG docker <username>


You will need to logout and back in again after doing so.


^^^^^^^^^^^^^^^^
Configure to AWS
^^^^^^^^^^^^^^^^

Authorising to TRACKIT Image Repo. Firstly, install AWS CLI tools. ::

	sudo apt-get install awscli


Configure with the relevant access keys, setting the default region to eu-west-1. ::

	aws configure


Log Docker into the repo: ::
   
	`aws ecr get-login --region eu-west-1`


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Install Database server (PostgreSQL)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PostgreSQL should be run on your native machine/VM: ::

   	sudo apt-get install postgresql
   	sudo -u postgres psql

	create user trackit with password 'tr4ck17';
    create database trackit with owner trackit;


Also install the PostGIS extension, currently this is 2.2, but adjust this for your Ubuntu version. ::

	sudo apt-get install postgresql-9.5-postgis-2.2

	sudo -u postgres psql
        	
	create extension postgis;
  

Enable local TCP connections to postgres: ::

	sudo nano /etc/postgresql/9.5/main/postgresql.conf


Find ``#listen_addresses="localhost"`` replace with: ``listen_addresses="*"`` ::

    sudo nano /etc/postgresql/9.5/main/pg_hba.conf

Add the following line to enable connections from docker IPs ::

	host  all   all   172.0.0.0/8   md5

	sudo service postgresql reload


^^^^^^^^^^^^^^^^^^^
Create Docker Swarm
^^^^^^^^^^^^^^^^^^^

Once the native postgres instance is set-up, we can need to create a swarm and overlay network for our services to communicate across. ::

	docker swarm init
	
	docker network create --driver overlay trackit

Then we can proceed to create services.

* Pull Images (Repeated Step)

There is a convenience script in trackit-docker bitbucket repo, path scripts/pull-from-ecr.py. This will pull the relevant images and tag them automatically. It can be repeatedly used.::
	
		vim ./pull-from-ecr.py
		
		chmod +x ./pull-from-ecr.py

		./pull-from-ecr.py


* App (Single Node Dev)

	[Optional] Setup Dev environment. You can use your local trackit code and virtualenv. This is highly recommended. (Ensure your SSH key has been added to bitbucket) 

	Create SSH key for Bitbucket

	Enter the following commands ::

		ssh-keygen

	Select default options ::

		cat ./home/ubuntu/.ssh/id_rsa.pub

	Copy the key and add to Bitbucket account.

	Then ::

	   	sudo mkdir /trackit

		sudo chown <youruser> /trackit

   		cd /trackit

		git clone git@bitbucket.org:trackit/trackit8.git

		cd /trackit8

		git checkout dev

		git submodule update --init
		
		git submodule foreach git checkout dev

		sudo apt-get install python-virtualenv

		virtualenv -p python3 /trackit/env-py3

		source /trackit/env-py3/bin/activate

		sudo apt-get -y install  libopenblas-dev libfreetype6-dev build-essential postgresql-server-dev-9.5 libcurl4-openssl-dev libxml2-dev libxslt-dev libjpeg-turbo8-dev linux-kernel-headers python3-dev

		pip install -r /trackit/trackit8/pip-requirements.txt

This will give you two folders on your machine (/trackit/trackit8 = dev code, /trackit/env-py3 = virtualenv) ::

 	docker service create --name app --publish 8000:8000 --network trackit -eTRACKIT_DB_HOST=<your host IP> -eTRACKIT_DB_USER=trackit -eTRACKIT_DB_PASS=trackit -eTRACKIT_DB_NAME=trackit -eDEVSERVER_OPTS="--settings=trackit.debug_settings" [--mount type=bind,source=/trackit/trackit8,destination=/trackit/trackit8 --mount type=bind,source=/trackit/env-py3,destination=/trackit/env-py3] trackit/appdev /bin/runserver.sh


Square bracket part is optional, and will bind mount your local development folders to the docker environment.

/bin/runserver.sh executes the django manage.py runserver script. If you do not specify this command, it will launch using Uwsgi.
  	
Devserver is bound to port 8000 on the host machine.

A convenience manage command is supplied within the image, which will activate the virtualenv and run manage.py, passing through any command line arguments.

^^^^^^^^^^^^^^^^^^^
Create the Services
^^^^^^^^^^^^^^^^^^^

* Create the Broker ::
	
	docker service create --name broker --network trackit trackit/brokerdev

* Queue

Enter the following ::

	docker service create --name queue --network trackit -eTRACKIT_DB_HOST=<your host IP> -eTRACKIT_DB_USER=trackit -eTRACKIT_DB_PASS=trackit -eTRACKIT_DB_NAME=trackit --mount type=bind,source=/trackit/trackit8,destination=/trackit/trackit8 trackit/queuedev


* Cleanup Queue
	
Enter the following ::

	docker service create --name cleanupqueue --network trackit -eTRACKIT_DB_HOST=<your host IP> -eTRACKIT_DB_USER=trackit -eTRACKIT_DB_PASS=trackit -eTRACKIT_DB_NAME=trackit --mount type=bind,source=/trackit/trackit8,destination=/trackit/trackit8 trackit/namedqueuedev


* Elasticsearch
	
Enter the following ::

	docker service create --name elasticsearch --network trackit trackit/elasticsearchdev


Install Docker as per standard Ubuntu instructions. Ensure you add your system user to the docker group: ::

   	usermod -aG docker <username>


You will need to logout and back in again after doing so.



.. _installation_steps:

---------------------------------
Installation Steps - Windows Node
---------------------------------

^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Install Reporting App (Logi)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install the Logi Reporting Service.


^^^^^^^^^^^^^^^^^^^^^^
Decompress the Archive
^^^^^^^^^^^^^^^^^^^^^^

Unzip the file, and add the Reporting folder to the appropriate location on the disk. i.e. ``C:\trackit8reporting``


^^^^^^^^^^^^^^^
Add Application
^^^^^^^^^^^^^^^

Within Logi, add an application, point to the Reporting folder and follow the Wizard. If appropriate, complete the IIS Install.


^^^^^^^^^^^
Install IIS
^^^^^^^^^^^

Install the IIS Role on the Windows machine, ensuring ASP.NET is checked.



^^^^^^^^^^^^^^
Check Settings
^^^^^^^^^^^^^^

Check the Settings file under ``C:\trackit8reporting\_Definitions\_Settings.lgx``, and ensure that the IP address of the Linux Application nodes have been added under ``AuthenticationClientAddresses``.


Once complete, browse to ``http:\\localhost\trackit8reporting`` to check the service has installed correctly.



.. _offline_installation:

==========================
Local Offline Installation
==========================

Should TRACKIT be installed into a secure environment where internet connectivity is not possible, TRACKIT provide an offline installer package. Please contact TRACKIT directly for assistance.
