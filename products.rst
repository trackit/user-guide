Products
========
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

================
Product Overview
================

Ensure that the :ref:`products`, :ref:`asset_class` and :ref:`categories` sections have been read and understood.

Products are fundamental to ensuring good quality data within TRACKIT. Correctly defining distinct Manufacturers, Models and Categories means data can be added, maintained and reported quickly and accurately.

To maintain Products, use the :ref:`product_wizard`.

======================
Maintaining Categories
======================

Categories can be maintained in the :ref:`product_wizard_category`.

The main purpose of Categories is to Group Products and Assets for Reporting.


=========================
Maintaining Manufacturers
=========================

Manufacturers can be maintained in the :ref:`product_wizard_manufacturer`.

It is possible to add Alias' for Manufacturers, i.e. Hewlett Packard, HP. This reduces the confusion and increases the consistency in the data.

By selecting a Manufacturer, it's possible to view all the Product Models created in the database using that Manufacturer.


==================
Maintaining Models
==================

Models can be maintained in the :ref:`product_wizard_model`.