Reports
=======
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

==================
Reporting Overview
==================

Reports

===============
Tabular Reports
===============


-------
Columns
-------

All data fields available against the report data, i.e. the Assets, Locations or Products are listed under Columns. By default, some Columns will be selected, however the User can show or hide any columns they choose.

Any EAV fields that have been configured within TRACKIT will also be listed and can be included in the report data.

.. figure:: ./reporting/reporting_columns.png
    :align: center
    :alt: Header Menu

    Columns can be selected or unselected.

-------
Formula
-------

Formulas allow the User to create a new field of data in the report based on the data columns available. 

It's possible to determine the Type of Data, i.e. date, number, text etc. and display the values in specific formats, as gauges or colours.

Any additional Formula columns created can be sorted, grouped, filtered etc as with any other fields.

Formulas are expressions made up of columns, constants, functions, and operators.

Columns are values that come from the data. Their names are are enclosed in square brackets, like [ShippedDate]. Depending on their data type, they may be used as text strings, numbers, and date/time values.

.. figure:: ./reporting/reporting_formula.png
    :align: center
    :alt: Header Menu

    An example of a Formula in the Reporting.

Here are some other examples::

	[Price] * [Quantity]

Multiply two data columns, Price and Quantity, to make an ExtendedPrice column::

	[Price] * .04

Multiply a data column by an constant value to calculate the tax applied to the price::

	DateDiff("d", [OrderDate], [ExpiryDate] )

Get the number of days from the order to the shipment::

	DateDiff("w", [ExpiryDate], Now )

Get the number of weekdays since the shipment date::

	WeekdayName( Weekday( [ExpiryDate] ) )

Return the name of the day of the week of the shipment date::

	[LastName] + ", " + [FirstName]

Concatenate columns and strings together. This might return: Smith, John::

	UCase( [LastName] + ", " + [FirstName] )

Convert to upper case. This might return: SMITH, JOHN


----
Sort
----

This option provides the functionality to Sort columns depending on the content. It's possible to sort on multiple columns in a specific order of priority.

.. figure:: ./reporting/reporting_sort.png
    :align: center
    :alt: Header Menu

    An example of sorting on multiple columns.



------
Filter
------

The filter function allows the User to display or hide specific data based on the data itself.

It's possible to filter on multiple columns and individual columns multiple times.

.. figure:: ./reporting/reporting_filter.png
    :align: center
    :alt: Header Menu

    An example of filtering on multiple values.

When filtering multiple times, selecting the And/Or text between the Filter row swaps the Filter rules from And (where the data being filtered must meet both requirements), or Or (where the data being filtered can meet either of the requirements).

The following types of Comparison operators are available:

* =, Equals, the data being filtered must match the given string exactly
* <, Less than, the data being filtered must be less than the given value
* <=, Less than or equal to, the data being filtered must be less than or equal to the given value
* >=, Greater than or equal to, the data being filtered must be greater than or equal to the given value
* > Greater than, the data being filtered must be greater than the given value
* Not =, The data returned will not exactly match the given string or value
* Starts With, The data returned will start with the given string
* Contains, The data returned will contain the given string
* Not Starts With, The data returned will not start with the given string
* Not Contains, The data returned will not contain the given string



-----
Group
-----


.. figure:: ./reporting/reporting_grouping.png
    :align: center
    :alt: Header Menu

    An example of grouping on multiple columns.


---------
Aggregate
---------


.. figure:: ./reporting/reporting_aggregate.png
    :align: center
    :alt: Header Menu

    An example of an aggregate value on grouped data.




.. figure:: ./reporting/reporting_aggregate_example.png
    :align: center
    :alt: Header Menu

    The results of an aggregate value on grouped data.



-----
Chart
-----




.. figure:: ./reporting/reporting_charts.png
    :align: center
    :alt: Header Menu

    A bar and pie chart, showing the same data, number of Asset per Product Manufacturer.



--------
Crosstab
--------




.. figure:: ./reporting/reporting_crosstab.png
    :align: center
    :alt: Header Menu

    An example of a crosstab showing the number of Assets per Parent Asset in a Location.



------
Paging
------




.. figure:: ./reporting/reporting_paging.png
    :align: center
    :alt: Header Menu

    A view of the Paging options.





==================
Saved/User Reports
==================








=============
Trend Reports
=============





=================
Dashboard Reports
=================
