Users and Groups
================
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

====================
Permissions Overview
====================



==============
Managing Users
==============

Users can be created, edited or removed in the :ref:`admin` Section. The following fields are required against a User:

* Email Address (Username)
* First Name
* Last Name

* Active - Users can be disabled to temporily or permanently revoke access but retain the User.
* Staff Status - Will the User have access to the Admin site
* Superuser status - Gives the User access to all permissions without explicitly assigning them.

* Groups - Lists the Groups the User is a member of.
* User Permissions - Individual Permissions granted to the User outside of any Group permissions they may inherit.


-------------------
Resetting Passwords
-------------------

It is possible to reset a User password through the Adminstration Menu of TRACKIT.


--------------
Deleting Users
--------------

Deleting a User from TRACKIT will remove the history and ability to restore the User. A more suitable option has been designed, where a User can be Suspended, but retained in the system for reporting and auditing.


==================
Maintaining Groups
==================

Groups can be added, edited or removed in the :ref:`admin` Section.

.. figure:: ./admin/groups.png
    :align: center
    :alt: Header Menu

    A view of the Group Permissions selector in TRACKIT Admin.

It's possible to select multiple permissions by holding down the Ctrl key and selecting a number of permissions.
