Wizards
=======
.. versionadded:: 16.02
.. versionchanged:: 17.02

.. image:: https://img.shields.io/badge/release-17.02-green.svg

===============
Wizard Overview
===============

There are a number of wizards in TRACKIT to assist users in completing common tasks and importing existing data.

Wizards can be accessed from the :ref:`account_menu`.

On selecting the Wizards menu, a dashboard is displayed summarising each section:

.. figure:: ./wizards/wizard_overview.png
    :align: center
    :alt: Header Menu

    A view of the Wizard Overview.


..
    .. _user_wizard:

    ===========
    User Wizard
    ===========

    These wizards a designed to assist in getting users and groups created within the application.

    -----
    Users
    -----

    Users can be created, edited or removed in this wizard. The following fields are required against a User:

    * First Name
    * Last Name
    * Email Address (Username)
    * Active - Users can be disabled to temporily or permanently revoke access but retain the User.
    * Groups - Lists the Groups the User is a member of.


    .. _user_groups:

    -----------
    User Groups
    -----------

    It is possible to search for, add, edit or remove Groups in this wizard. Permissions are defined against a Group, so Users must be a member of a Group to inherit the properies or permissions.


.. _location_wizard:

===============
Location Wizard
===============

TRACKIT has the ability to define locations and structure these however required. A Location could be a physical one, such as a Building or a Cupboard, or a Logical container, such as a Department or Virtual Container. The following sections describes how each Location attribute works.

---------
Locations
---------

The Location Wizard allows the User to search for, add, view, edit and delete locations.

It displays the following information:

	* Location Name
	* Location Type
	* Root Location - Whether the Location can be the top most parent in the Location Tree
	* Allow Assets - Whether it's possible to add Assets to the Location
	* Children - When the Location has child Locations, and a link to view any.
	* Parent - The name and a link to the Parent Location.


.. _location_type:

--------------
Location Types
--------------

Locations Types can be configured to help group, search or define different types of Locations. 

Location Types contain the following fields:

	* Name
	* Attributes - the pre-existing fields available
	* Allowed child types - which Location types can be added as children of this Location type, for instance a Building would allow a Room child type, but a Room would not allow a Building to be a child.
	* Allow Device Placement - can Assets be placed within this Location, or is it effectively just a structural or group for other Locations.
	* Require Postal Information - this Location Type requires Users to add an address when creating Locations using this type.
	* Map Marker Colour - select the colour for this Location Type to be shown where mapping is viewable.
	* Marker Icon - no longer relevant.


.. _location_tree:

-------------
Location Tree
-------------

This gives an overview of the current Location structure within TRACKIT. 

It is possible to use this view to amend the Location structure.


.. _product_wizard:

==============
Product Wizard
==============


.. _product_wizard_category:

----------
Categories
----------

Categories are listed alphabetically, with all child Categories shown alongside each.

It is possible to add new, edit existing or remove Categories.

.. _product_wizard_manufacturer:

-------------
Manufacturers
-------------

It is possible to search for, add, modify or remove Product Manufacturers from this wizard.

Information about Manufacturers are shown in this wizard, they include:

	* Name
	* Alias - Other known names for the Manufacturer
	* Models - The number of Product Models using the Manufacturer

By selecting a Manufacturer, it's possible to view all Models and edit each directly if required.

.. _product_wizard_model:

------
Models
------

This wizard provides the ability to search for (either Simple or Advanced), add, modify and update Product Models within TRACKIT.

The Advanced Search function allows the User to search across multiple Manufacturers, Product Families and/or Asset Classes.



-------------
Import Models
-------------

This Wizard allows Users to import a list of Models via a text based CSV file. There is no fixed format to this file, it can be defined by the User and any columns contained in the file can either be matched to a field in TRACKIT or ignored.

There are 7 stages to this import.

1. Choose File - Select and upload a CSV file from the Users local machine.
2. Match Columns - TRACKIT will identify the columns of data contained in the file and prompt the User to match them to a Model field within TRACKIT.

.. figure:: ./wizards/model_import_load.png
    :align: center
    :alt: Header Menu

    An example of an analysed CSV.

3. Match Asset Classes - TRACKIT will attempt to match Asset Classes to existing Classes within TRACKIT or request the User make the match.

.. figure:: ./wizards/model_import_class.png
    :align: center
    :alt: Header Menu

    TRACKIT attempts to match values based on names, this can be overridden.

4. Match Manufacturers - TRACKIT will attempt to match the Manufacturer names in the CSV to existing Manufacturers in TRACKIT. It is not possible to create new Manufacturers automatically.
5. Match Categories - TRACKIT will attempt to match Categories to existing Categories in TRACKIT. It is not possible to create new Categories automatically.
6. Validate - TRACKIT will review the data to see if there is likely to be any duplication or conflict should the data be loaded. Any warnings are displayed.

.. figure:: ./wizards/model_import_validate.png
    :align: center
    :alt: Header Menu

    Any validation errors or warnings are shown.

7. Import - Any data which Validated successfully will attempt to be imported. Links to successfully loaded Models are created and displayed, any errors are listed at the bottom.


.. figure:: ./wizards/model_import_import.png
    :align: center
    :alt: Header Menu

    An example of a Imported result set.

Completing this Wizard prior to importing an existing Asset Inventory is highly recommended.


============
Asset Wizard
============

------------
Asset Groups
------------

This form allows the User to view, edit or remove existing Asset Groups or define new ones.

Each Asset Group has a colour defined, which is used in various parts of the application to help identify specific Asset Groups.


-----------------
Asset Group Types
-----------------

The Asset Groups Types Wizard provide a form to create the Types available for Asset Groups.


.. _asset_import_wizard:

------------
Asset Import
------------

This Wizard allows Users to import a list of Assets via a text based CSV file. There is no fixed format to this file, it can be defined by the User and any columns contained in the file can either be matched to a field in TRACKIT, ignored or a new EAV field created.

There are 7 stages to this import.

1. Choose File - Select and upload a CSV file from the Users local machine.
2. Match Columns - TRACKIT will identify the columns of data contained in the file and prompt the User to match them to an Asset field within TRACKIT, ignore the field, or create a new field.

.. figure:: ./wizards/asset_import_load.png
    :align: center
    :alt: Header Menu

    An example of an analysed Asset CSV.

3. Match Locations - TRACKIT will attempt to match Location data to existing Locations within TRACKIT using the name. It's possible to load Assets into the system, but against a PLACEHOLDER Location. This is not recommended, Locations should be defined first.

.. figure:: ./wizards/asset_import_location.png
    :align: center
    :alt: Header Menu

    TRACKIT attempts to match values based on names, this can be override.

4. Match Models - TRACKIT will attempt to match the Product Models in the CSV to existing Product Models in TRACKIT. It is not possible to create new Models automatically, so Models should be loaded into the application prior to running this wizard.

.. figure:: ./wizards/asset_import_models.png
    :align: center
    :alt: Header Menu

    TRACKIT attempts to match values based on names, this can be overridden.

5. Match Parent Assets - TRACKIT will attempt to match the Parent Assets based on the earlier selected Parent field. Parents should be included in the CSV data.

6. Validate - TRACKIT will review the data to see if there is likely to be any duplication or conflict should the data be loaded. Any warnings are displayed.

.. figure:: ./wizards/asset_import_validate.png
    :align: center
    :alt: Header Menu

    Any validation errors or warnings are shown.

7. Import - Any data which Validated successfully will attempt to be imported. Links to successfully loaded Models are created and displayed, any errors are listed at the bottom.


.. figure:: ./wizards/asset_import_import.png
    :align: center
    :alt: Header Menu

    An example of a Imported result set.




==========
EAV Wizard
==========

----------
EAV Groups
----------

This wizard allows Users to add and edit EAV Groups.

----------
EAV Fields
----------

EAV fields can be added, edited or removed in this Wizard. 

